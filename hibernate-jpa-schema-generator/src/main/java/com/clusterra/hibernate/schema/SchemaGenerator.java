/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.hibernate.schema;

import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.DefaultResourceLoader;

import java.util.Set;

/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
public class SchemaGenerator {

    private static Logger logger = LoggerFactory.getLogger(SchemaGenerator.class);

    public static void main(String[] args) throws Exception {

        if (args.length != 3) {
            System.out.println("Arguments required (with proper order):");
            System.out.println("argument 0: packageSearchPath, i.e. classpath:com.clusterra/**/domain/model/**/*.class");
            System.out.println("argument 1: outputFile, i.e. ./project-name-ddl.sql");
            System.out.println("argument 2: dialect, i.e. org.hibernate.dialect.PostgreSQL9Dialect");
            System.exit(0);
        }

        String packageSearchPath = args[0];
        String outputFile = args[1];
        String dialect = args[2];

        Configuration configuration = new Configuration();
        configuration.setProperty("hibernate.dialect", dialect);

        Set<Class> persistentClasses = new JPAClassesScanner(packageSearchPath, new DefaultResourceLoader()).scan();


        logger.info("total classes scanned for ddl generation: ", persistentClasses.size());
        for (Class clazz : persistentClasses) {
            logger.info("class scanned for ddl generation: {}", clazz.getName());
            configuration.addAnnotatedClass(clazz);
        }

        SchemaExport schema = new SchemaExport(configuration);
        schema.setOutputFile(outputFile);
        schema.setDelimiter(";");
        schema.execute(false, false, false, true);
    }

}
