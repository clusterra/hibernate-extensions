/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.hibernate.schema;


import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternUtils;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.core.type.filter.TypeFilter;

import javax.persistence.Embeddable;
import javax.persistence.Entity;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Alexander Semenov (Jabber ID: bohtvaroh@jabby.org)
 * @author Alexander Fyodorov
 */
public class JPAClassesScanner {

    /**
     * Base package to scan.
     */
    private final String packageSearchPath;


    private final Class<Entity> ENTITY_ANNOTATION = Entity.class;
    private final Class<Embeddable> EMBEDDABLE_ANNOTATION = Embeddable.class;

    private final ResourcePatternResolver resourcePatternResolver;
    private final MetadataReaderFactory metadataReaderFactory;
    private final TypeFilter entityAnnotationFilter = new AnnotationTypeFilter(ENTITY_ANNOTATION);
    private final TypeFilter embeddableAnnotationFilter = new AnnotationTypeFilter(EMBEDDABLE_ANNOTATION);


    public JPAClassesScanner(String packageSearchPath, ResourceLoader resourceLoader) {
        this.packageSearchPath = packageSearchPath;
        this.resourcePatternResolver = ResourcePatternUtils.getResourcePatternResolver(resourceLoader);
        this.metadataReaderFactory = new CachingMetadataReaderFactory(resourceLoader);
    }


    /**
     * Looks for persistent classes in the classpath under the specified packages.
     *
     * @return set of found classes
     */
    public Set<Class> scan() {
        Set<Class> result = new HashSet<Class>();

        try {

            Resource[] resources = resourcePatternResolver.getResources(packageSearchPath);

            for (Resource resource : resources) {
                if (resource.isReadable()) {
                    MetadataReader metadataReader = this.metadataReaderFactory.getMetadataReader(resource);

                    if (this.entityAnnotationFilter.match(metadataReader, metadataReaderFactory) ||
                            this.embeddableAnnotationFilter.match(metadataReader, metadataReaderFactory)) {
                        result.add(Class.forName(metadataReader.getAnnotationMetadata().getClassName()));
                    }
                }
            }
        } catch (IOException | ClassNotFoundException ex) {
            throw new RuntimeException("failure during jpa classes scanning", ex);
        }

        return result;
    }

}
